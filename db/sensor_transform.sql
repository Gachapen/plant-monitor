CREATE TABLE sensor_transform (
    sensor_id integer NOT NULL,
    min_value double precision NOT NULL,
    max_value double precision NOT NULL,
    PRIMARY KEY(sensor_id),
    CONSTRAINT fk_sensor
      FOREIGN KEY(sensor_id)
        REFERENCES sensor(id)
);
