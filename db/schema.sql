--
-- PostgreSQL database dump
--

-- Dumped from database version 11.7 (Raspbian 11.7-0+deb10u1)
-- Dumped by pg_dump version 11.7 (Raspbian 11.7-0+deb10u1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: machine; Type: TABLE; Schema: public; Owner: plantmonitor
--

CREATE TABLE public.machine (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.machine OWNER TO plantmonitor;

--
-- Name: machine_id_seq; Type: SEQUENCE; Schema: public; Owner: plantmonitor
--

ALTER TABLE public.machine ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.machine_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: module; Type: TABLE; Schema: public; Owner: plantmonitor
--

CREATE TABLE public.module (
    id integer NOT NULL,
    name character varying NOT NULL,
    machine_id integer NOT NULL
);


ALTER TABLE public.module OWNER TO plantmonitor;

--
-- Name: module_id_seq; Type: SEQUENCE; Schema: public; Owner: plantmonitor
--

ALTER TABLE public.module ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.module_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: reading; Type: TABLE; Schema: public; Owner: plantmonitor
--

CREATE TABLE public.reading (
    id integer NOT NULL,
    sensor_id integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    value double precision NOT NULL,
    aggregation smallint DEFAULT 0 NOT NULL
);


ALTER TABLE public.reading OWNER TO plantmonitor;

--
-- Name: reading_id_seq; Type: SEQUENCE; Schema: public; Owner: plantmonitor
--

ALTER TABLE public.reading ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.reading_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sensor; Type: TABLE; Schema: public; Owner: plantmonitor
--

CREATE TABLE public.sensor (
    id integer NOT NULL,
    module_id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.sensor OWNER TO plantmonitor;

--
-- Name: sensor_id_seq; Type: SEQUENCE; Schema: public; Owner: plantmonitor
--

ALTER TABLE public.sensor ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.sensor_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: sensor_transform; Type: TABLE; Schema: public; Owner: plantmonitor
--

CREATE TABLE public.sensor_transform (
    sensor_id integer NOT NULL,
    min_value double precision NOT NULL,
    max_value double precision NOT NULL
);


ALTER TABLE public.sensor_transform OWNER TO plantmonitor;

--
-- Name: machine machine_pk; Type: CONSTRAINT; Schema: public; Owner: plantmonitor
--

ALTER TABLE ONLY public.machine
    ADD CONSTRAINT machine_pk PRIMARY KEY (id);


--
-- Name: module module_pkey; Type: CONSTRAINT; Schema: public; Owner: plantmonitor
--

ALTER TABLE ONLY public.module
    ADD CONSTRAINT module_pkey PRIMARY KEY (id);


--
-- Name: reading reading_pkey; Type: CONSTRAINT; Schema: public; Owner: plantmonitor
--

ALTER TABLE ONLY public.reading
    ADD CONSTRAINT reading_pkey PRIMARY KEY (id);


--
-- Name: reading reading_sensor_id_timestamp_aggregation_uq; Type: CONSTRAINT; Schema: public; Owner: plantmonitor
--

ALTER TABLE ONLY public.reading
    ADD CONSTRAINT reading_sensor_id_timestamp_aggregation_uq UNIQUE (sensor_id, "timestamp", aggregation);


--
-- Name: sensor sensor_pkey; Type: CONSTRAINT; Schema: public; Owner: plantmonitor
--

ALTER TABLE ONLY public.sensor
    ADD CONSTRAINT sensor_pkey PRIMARY KEY (id);


--
-- Name: sensor_transform sensor_transform_pkey; Type: CONSTRAINT; Schema: public; Owner: plantmonitor
--

ALTER TABLE ONLY public.sensor_transform
    ADD CONSTRAINT sensor_transform_pkey PRIMARY KEY (sensor_id);


--
-- Name: reading_aggregation_index; Type: INDEX; Schema: public; Owner: plantmonitor
--

CREATE INDEX reading_aggregation_index ON public.reading USING btree (aggregation);


--
-- Name: reading_sensor_id_index; Type: INDEX; Schema: public; Owner: plantmonitor
--

CREATE INDEX reading_sensor_id_index ON public.reading USING btree (sensor_id);


--
-- Name: reading_sensor_id_timestamp_aggregation_index; Type: INDEX; Schema: public; Owner: plantmonitor
--

CREATE INDEX reading_sensor_id_timestamp_aggregation_index ON public.reading USING btree (sensor_id, "timestamp", aggregation);


--
-- Name: reading_timestamp_index; Type: INDEX; Schema: public; Owner: plantmonitor
--

CREATE INDEX reading_timestamp_index ON public.reading USING btree ("timestamp");


--
-- Name: sensor_transform fk_sensor; Type: FK CONSTRAINT; Schema: public; Owner: plantmonitor
--

ALTER TABLE ONLY public.sensor_transform
    ADD CONSTRAINT fk_sensor FOREIGN KEY (sensor_id) REFERENCES public.sensor(id);


--
-- Name: module module_machine_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plantmonitor
--

ALTER TABLE ONLY public.module
    ADD CONSTRAINT module_machine_id_fkey FOREIGN KEY (machine_id) REFERENCES public.machine(id);


--
-- Name: reading reading_sensor_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plantmonitor
--

ALTER TABLE ONLY public.reading
    ADD CONSTRAINT reading_sensor_id_fkey FOREIGN KEY (sensor_id) REFERENCES public.sensor(id);


--
-- Name: sensor sensor_module_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: plantmonitor
--

ALTER TABLE ONLY public.sensor
    ADD CONSTRAINT sensor_module_id_fkey FOREIGN KEY (module_id) REFERENCES public.module(id);


--
-- PostgreSQL database dump complete
--

