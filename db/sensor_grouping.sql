CREATE TABLE sensor_grouping (
    sensor_id integer NOT NULL,
    group_name character varying NOT NULL,
    PRIMARY KEY(sensor_id, group_name),
    CONSTRAINT fk_sensor
      FOREIGN KEY(sensor_id)
        REFERENCES sensor(id)
);
