# Plant Monitor

## Setup

### Ubuntu

```
sudo apt install build-essential gcc-arm-linux-gnueabihf python3 python3-pip libpq-dev
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

### Debian

```
sudo apt install armv7-unknown-linux-gnueabihf rsync rustup python3 python3-pip libpq-dev
```

### All

```
rustup target add armv7-unknown-linux-gnueabihf
pip3 install -r monitor/requirements.txt
```

## Deploy

```
./deploy.sh
```
