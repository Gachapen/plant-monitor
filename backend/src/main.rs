use crate::aggregation::start_data_aggregation;
use sqlx::postgres::PgPool;
use std::env;
use warp::Filter;

mod aggregation;
mod db;
mod filters;
mod handlers;
mod models;

#[tokio::main]
async fn main() -> Result<(), sqlx::Error> {
    if env::var_os("RUST_LOG").is_none() {
        env::set_var("RUST_LOG", "warn,plant_monitor=info,pm-web=info");
    }
    pretty_env_logger::init();

    let port = env::var_os("WARP_PORT")
        .map(|var| var.to_str().unwrap().parse::<u16>().unwrap())
        .unwrap_or(3030);

    let db_pool = PgPool::connect(
        &"postgresql://plantmonitor:738LbZD4cT6dYSnSjDtQwil5@localhost/plantmonitor",
    )
    .await?;

    let api = filters::sensors(db_pool.clone());
    let cors = warp::cors()
        .allow_any_origin()
        .allow_methods(vec!["GET", "PUT", "POST"])
        .allow_headers(vec!["Content-Type"]);

    // View access logs by setting `RUST_LOG=plantmonitor`.
    let routes = api.with(cors).with(warp::log("pm-web"));

    start_data_aggregation(db_pool);

    // Start up the server...
    warp::serve(routes).run(([0, 0, 0, 0], port)).await;

    Ok(())
}
