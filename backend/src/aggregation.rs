use crate::db;
use chrono::prelude::*;
use chrono::Duration;
use log::{debug, info};
use serde_derive::Deserialize;
use sqlx::postgres::PgPool;
use std::fmt;

#[repr(i16)]
#[derive(Copy, Clone, sqlx::Type, Deserialize)]
pub enum Aggregation {
    None = 0,
    MinutelyAverage = 1,
    HourlyAverage = 2,
    DailyAverage = 3,
}

impl fmt::Display for Aggregation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Aggregation::None => write!(f, "no aggregation"),
            Aggregation::MinutelyAverage => write!(f, "average per minute"),
            Aggregation::HourlyAverage => write!(f, "hourly average"),
            Aggregation::DailyAverage => write!(f, "daily average"),
        }
    }
}

impl Aggregation {
    fn get_duration(&self) -> Duration {
        match self {
            Aggregation::None => Duration::zero(),
            Aggregation::MinutelyAverage => Duration::minutes(1),
            Aggregation::HourlyAverage => Duration::hours(1),
            Aggregation::DailyAverage => Duration::days(1),
        }
    }

    fn get_start_time(&self, time: &DateTime<Utc>) -> DateTime<Utc> {
        match self {
            Aggregation::None => time.clone(),
            Aggregation::MinutelyAverage => time
                .date()
                .and_time(NaiveTime::from_hms(time.hour(), time.minute(), 0))
                .unwrap(),
            Aggregation::HourlyAverage => time
                .date()
                .and_time(NaiveTime::from_hms(time.hour(), 0, 0))
                .unwrap(),
            Aggregation::DailyAverage => {
                time.date().and_time(NaiveTime::from_hms(0, 0, 0)).unwrap()
            }
        }
    }

    // fn get_end_time(&self, time: &DateTime<Utc>) -> DateTime<Utc> {
    //     let start_time = self.get_start_time(time);
    //     match self {
    //         Aggregation::None => start_time,
    //         Aggregation::MinutelyAverage => start_time + Duration::minutes(1),
    //         Aggregation::HourlyAverage => start_time + Duration::hours(1),
    //         Aggregation::DailyAverage => start_time + Duration::days(1),
    //     }
    // }
}

pub fn start_data_aggregation(db_pool: PgPool) {
    use std::time::Duration;
    use tokio::time::sleep;

    let aggregation_interval = Duration::from_secs(60);

    info!("Starting aggregation");
    tokio::spawn(async move {
        loop {
            aggregate_data(&db_pool).await.unwrap();
            sleep(aggregation_interval).await;
        }
    });
}

async fn aggregate_data(db_pool: &PgPool) -> sqlx::Result<()> {
    info!("Aggregating...");

    let db_sensors: Vec<(i32, String)> = sqlx::query_as(
        r#"
        SELECT sensor.id, sensor.name
        FROM sensor
        ORDER BY sensor.id ASC
    "#,
    )
    .fetch_all(db_pool)
    .await
    .unwrap();

    for (sensor_id, sensor_name) in db_sensors.into_iter() {
        debug!(
            "Running aggregation for sensor {} '{}'",
            sensor_id, sensor_name
        );
        aggregate_sensor_data(db_pool, sensor_id, Aggregation::DailyAverage).await?;
        aggregate_sensor_data(db_pool, sensor_id, Aggregation::HourlyAverage).await?;
        aggregate_sensor_data(db_pool, sensor_id, Aggregation::MinutelyAverage).await?;
    }

    Ok(())
}

async fn aggregate_sensor_data(
    db_pool: &PgPool,
    sensor_id: i32,
    aggregation: Aggregation,
) -> sqlx::Result<()> {
    debug!("Running {} aggregation", aggregation);

    let aggregation_length = aggregation.get_duration();

    let time_of_last_aggregation =
        get_last_aggregation_timestamp(db_pool, sensor_id, aggregation).await?;

    let mut aggregation_start = if let Some(time_of_last_aggregation) = time_of_last_aggregation {
        debug!("Starting aggregation from previous aggregation");
        time_of_last_aggregation + aggregation_length
    } else {
        debug!("Starting aggregation from first reading");

        let first_reading = db::get_first_reading(db_pool, sensor_id).await;
        if let Some(reading) = first_reading {
            let time_of_first_reading = DateTime::from_utc(reading.timestamp, Utc);
            aggregation.get_start_time(&time_of_first_reading)
        } else {
            return Ok(());
        }
    };

    let max_time_back = Duration::weeks(1);
    let now = Utc::now();
    let earliest_aggreation_start = aggregation.get_start_time(&(now - max_time_back));

    if aggregation_start < earliest_aggreation_start {
        debug!(
            "Skipping readings between {} and {} because of long time period",
            &aggregation_start, &earliest_aggreation_start
        );
        aggregation_start = earliest_aggreation_start;
    }

    debug!("Aggregation start: {}", aggregation_start);

    let last_reading = db::get_last_reading(db_pool, sensor_id).await;
    let time_of_last_reading = DateTime::<Utc>::from_utc(last_reading.timestamp, Utc);
    let aggregation_end = aggregation.get_start_time(&time_of_last_reading);

    debug!("Aggregation end: {}", aggregation_end);

    let mut num_saved: u32 = 0;
    let mut num_skipped: u32 = 0;
    while aggregation_start < aggregation_end {
        let has_reading =
            add_average_reading(db_pool, sensor_id, aggregation_start, aggregation).await?;
        if has_reading {
            num_saved += 1;
            debug!(
                "Saved {} aggregation {} for sensor {} at {}",
                aggregation, num_saved, sensor_id, aggregation_start
            );
        } else {
            num_skipped += 1;
            debug!(
                "No readings for {} aggregation {} for sensor {} at {}",
                aggregation, num_saved, sensor_id, aggregation_start
            );
        }
        aggregation_start = aggregation_start + aggregation_length;
    }

    info!(
        "Saved {} and skipped {} {} aggregations for sensor {} at {}",
        num_saved, num_skipped, aggregation, sensor_id, aggregation_start
    );

    Ok(())
}

// async fn get_readings_during(
//     db_pool: &PgPool,
//     sensor_id: i32,
//     start: DateTime<Utc>,
//     end: DateTime<Utc>,
// ) -> Vec<db::Reading> {
//     sqlx::query_as(
//         r#"
//         SELECT sensor_id, timestamp, value
//         FROM reading
//         WHERE sensor_id = $1 AND timestamp >= $2 AND timestamp < $3
//         ORDER BY timestamp DESC
//     "#,
//     )
//     .bind(sensor_id)
//     .bind(start)
//     .bind(end)
//     .fetch_all(db_pool)
//     .await
//     .unwrap()
// }

async fn get_average_reading_during(
    db_pool: &PgPool,
    sensor_id: i32,
    start: DateTime<Utc>,
    end: DateTime<Utc>,
) -> sqlx::Result<Option<f64>> {
    let result: (Option<f64>,) = sqlx::query_as(
        r#"
        SELECT AVG(value)
        FROM reading
        WHERE sensor_id = $1 AND timestamp >= $2 AND timestamp < $3
    "#,
    )
    .bind(sensor_id)
    .bind(start)
    .bind(end)
    .fetch_one(db_pool)
    .await?;

    Ok(result.0)
}

async fn add_average_reading(
    db_pool: &PgPool,
    sensor_id: i32,
    start: DateTime<Utc>,
    aggregation: Aggregation,
) -> sqlx::Result<bool> {
    let end = match aggregation {
        Aggregation::None => return Ok(false),
        Aggregation::MinutelyAverage => start + Duration::minutes(1),
        Aggregation::HourlyAverage => start + Duration::hours(1),
        Aggregation::DailyAverage => start + Duration::days(1),
    };

    let average = get_average_reading_during(db_pool, sensor_id, start, end).await?;
    if let Some(average) = average {
        sqlx::query(
            r#"
            INSERT INTO reading (sensor_id, timestamp, value, aggregation)
            VALUES ($1, $2, $3, $4)
        "#,
        )
        .bind(sensor_id)
        .bind(start)
        .bind(average)
        .bind(aggregation)
        .execute(db_pool)
        .await?;

        Ok(true)
    } else {
        Ok(false)
    }
}

async fn get_last_aggregation_timestamp(
    db_pool: &PgPool,
    sensor_id: i32,
    aggregation: Aggregation,
) -> sqlx::Result<Option<DateTime<Utc>>> {
    let result: Option<(NaiveDateTime,)> = sqlx::query_as(
        r#"
        SELECT timestamp
        FROM reading
        WHERE sensor_id = $1 AND aggregation = $2
        ORDER BY timestamp DESC
        LIMIT 1
    "#,
    )
    .bind(sensor_id)
    .bind(aggregation)
    .fetch_optional(db_pool)
    .await?;

    Ok(result.map(|(timestamp,)| DateTime::from_utc(timestamp, Utc)))
}

pub async fn get_last_aggregated_reading(
    sensor_id: i32,
    aggregation: Aggregation,
    db: &PgPool,
) -> sqlx::Result<(NaiveDateTime, f64)> {
    let reading: (NaiveDateTime, f64) = sqlx::query_as(
        r#"
        SELECT timestamp, value
        FROM reading
        WHERE sensor_id = $1 AND aggregation = $2
        ORDER BY timestamp DESC
        LIMIT 1;
    "#,
    )
    .bind(sensor_id)
    .bind(aggregation)
    .fetch_one(db)
    .await?;

    Ok(reading)
}
