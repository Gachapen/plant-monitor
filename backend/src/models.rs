use chrono::{DateTime, Utc};
use serde_derive::{Deserialize, Serialize};

use crate::aggregation::Aggregation;

#[derive(Serialize)]
pub struct Sensor {
    pub id: i32,
    pub name: String,
    pub transform: Option<SensorTransform>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct SensorTransform {
    pub min_value: f64,
    pub max_value: f64,
}

#[derive(Serialize)]
pub(crate) struct SensorReadings {
    pub(crate) name: String,
    pub(crate) readings: Vec<Reading>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub(crate) struct Reading {
    pub(crate) timestamp: DateTime<Utc>,
    pub(crate) value: f64,
    pub(crate) raw_value: f64,
}

#[derive(Deserialize, Debug)]
pub struct SensorUpdateRequest {
    pub name: String,
    pub transform: Option<SensorTransform>,
}

#[derive(Deserialize)]
pub struct SensorReadingsQuery {
    pub after: DateTime<Utc>,
    pub aggregation: Option<Aggregation>,
}

impl SensorTransform {
    pub fn transform(&self, value: f64) -> f64 {
        (value - self.min_value) / (self.max_value - self.min_value)
    }
}
