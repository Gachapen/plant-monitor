use crate::models::{SensorTransform, SensorUpdateRequest};
use crate::{db, models::Sensor};
use sqlx::postgres::PgPool;
use std::convert::Infallible;
use warp::hyper::StatusCode;

pub async fn get_sensors(db_pool: PgPool) -> Result<impl warp::Reply, Infallible> {
    let db_sensors = db::get_sensors(&db_pool).await.unwrap();

    let sensors: Vec<_> = db_sensors
        .into_iter()
        .map(|s| Sensor {
            id: s.id,
            name: s.name,
            transform: s.transform.map(|t| SensorTransform {
                min_value: t.min_value,
                max_value: t.max_value,
            }),
        })
        .collect();

    Ok(warp::reply::json(&sensors))
}

pub async fn update_sensor(
    id: i32,
    sensor: SensorUpdateRequest,
    db_pool: PgPool,
) -> Result<impl warp::Reply, Infallible> {
    sqlx::query(
        r#"
        UPDATE sensor SET
            name = $2
        WHERE sensor.id = $1;
    "#,
    )
    .bind(id)
    .bind(&sensor.name)
    .execute(&db_pool)
    .await
    .unwrap();

    if let Some(transform) = &sensor.transform {
        upsert_sensor_transform(id, transform, &db_pool)
            .await
            .unwrap();
    } else {
        remove_sensor_transform(id, &db_pool).await.unwrap();
    }

    Ok(StatusCode::NO_CONTENT)
}

async fn upsert_sensor_transform(
    sensor_id: i32,
    transform: &SensorTransform,
    db_pool: &PgPool,
) -> Result<(), sqlx::Error> {
    sqlx::query(
        r#"
        INSERT INTO sensor_transform (sensor_id, min_value, max_value)
        VALUES($1, $2, $3)
        ON CONFLICT (sensor_id)
        DO UPDATE SET
                min_value = $2,
                max_value = $3;
    "#,
    )
    .bind(sensor_id)
    .bind(transform.min_value)
    .bind(transform.max_value)
    .execute(db_pool)
    .await?;

    Ok(())
}

async fn remove_sensor_transform(sensor_id: i32, db_pool: &PgPool) -> Result<(), sqlx::Error> {
    sqlx::query(
        r#"
        DELETE FROM sensor_transform
        WHERE sensor_id = $1;
    "#,
    )
    .bind(sensor_id)
    .execute(db_pool)
    .await?;

    Ok(())
}
