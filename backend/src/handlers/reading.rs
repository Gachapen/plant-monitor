use crate::{
    aggregation::Aggregation,
    models::{Reading, SensorReadings},
};
use crate::{db, models::SensorReadingsQuery};
use chrono::prelude::*;
use sqlx::postgres::PgPool;
use std::{collections::HashMap, convert::Infallible};

pub async fn readings(db_pool: PgPool) -> Result<impl warp::Reply, Infallible> {
    let db_sensors: Vec<(i32, String)> = sqlx::query_as(
        r#"
        SELECT sensor.id, sensor.name
        FROM sensor;
    "#,
    )
    .fetch_all(&db_pool)
    .await
    .unwrap();

    let db_readings: Vec<(i32, NaiveDateTime, f64)> = sqlx::query_as(r#"
        WITH _readings as (
            SELECT sensor_id, timestamp, value, ROW_NUMBER() OVER (PARTITION BY sensor_id ORDER BY timestamp DESC) AS row_number
            FROM reading
            WHERE aggregation = 1
        )
        
        SELECT sensor_id, timestamp, value
        FROM _readings
        WHERE row_number <= 600
        ORDER BY timestamp ASC
    "#)
        .fetch_all(&db_pool).await.unwrap();

    let mut sensor_readings_map = HashMap::<i32, Vec<Reading>>::new();
    for (sensor_id, timestamp, value) in db_readings {
        sensor_readings_map
            .entry(sensor_id)
            .or_default()
            .push(Reading {
                timestamp: DateTime::from_utc(timestamp, Utc),
                value: value,
                raw_value: value,
            });
    }

    let mut sensor_readings = Vec::<SensorReadings>::new();
    for (sensor_id, readings) in sensor_readings_map.into_iter() {
        sensor_readings.push(SensorReadings {
            name: db_sensors
                .iter()
                .find(|(id, _)| *id == sensor_id)
                .unwrap()
                .1
                .clone(),
            readings: readings,
        });
    }

    Ok(warp::reply::json(&sensor_readings))
}

pub async fn get_latest_reading(
    sensor_id: i32,
    db_pool: PgPool,
) -> Result<impl warp::Reply, Infallible> {
    let reading = db::get_latest_reading(sensor_id, &db_pool).await.unwrap();
    let transform = db::get_sensor_transform(sensor_id, &db_pool).await.unwrap();
    return Ok(warp::reply::json(&Reading {
        timestamp: DateTime::from_utc(reading.timestamp, Utc),
        value: transform.map_or(reading.value, |t| t.transform(reading.value)),
        raw_value: reading.value,
    }));
}

pub async fn get_sensor_readings(
    sensor_id: i32,
    query: SensorReadingsQuery,
    db: PgPool,
) -> Result<impl warp::Reply, Infallible> {
    let transform = db::get_sensor_transform(sensor_id, &db).await.unwrap();
    let db_readings = db::get_readings_after(
        sensor_id,
        query.after,
        query.aggregation.unwrap_or(Aggregation::None),
        &db,
    )
    .await
    .unwrap();

    let readings: Vec<_> = db_readings
        .iter()
        .map(|r| Reading {
            timestamp: DateTime::from_utc(r.timestamp, Utc),
            raw_value: r.value,
            value: transform.as_ref().map_or(r.value, |t| t.transform(r.value)),
        })
        .collect();

    Ok(warp::reply::json(&readings))
}
