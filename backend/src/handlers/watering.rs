use std::convert::Infallible;
use log::info;

use crate::{
    aggregation::{get_last_aggregated_reading, Aggregation},
    db,
};
use sqlx::PgPool;

pub async fn should_water(db: PgPool) -> Result<impl warp::Reply, Infallible> {
    let sensors = db::get_sensors_in_group("watering", &db).await.unwrap();

    let mut readings = Vec::new();
    let aggregation = Aggregation::MinutelyAverage;

    for sensor in sensors {
        let (timestamp, value) = get_last_aggregated_reading(sensor.id, aggregation, &db)
            .await
            .unwrap();
        let transformed = sensor.transform.map_or(value, |t| t.transform(value));
        readings.push((timestamp, transformed));
    }

    let readings = readings;
    let num_readings = readings.len() as f64;

    let average_reading = readings.iter().map(|(_, value)| value).sum::<f64>() / num_readings;
    let should_water = if average_reading > 0.6 { true } else { false };

    info!("Should water: {} ({}, {})", should_water, average_reading, num_readings);

    Ok(warp::reply::json(&should_water))
}
