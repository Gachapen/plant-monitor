use chrono::{DateTime, NaiveDateTime, Utc};
use sqlx::PgPool;

use crate::{aggregation::Aggregation, models::{Sensor, SensorTransform}};

#[derive(sqlx::FromRow)]
pub(crate) struct Reading {
    pub timestamp: NaiveDateTime,
    pub value: f64,
}

#[derive(sqlx::FromRow)]
struct SensorRow {
    id: i32,
    name: String,
    min_value: Option<f64>,
    max_value: Option<f64>,
}

impl Into<Sensor> for SensorRow {
    fn into(self) -> Sensor {
        Sensor {
            id: self.id,
            name: self.name,
            transform: if self.min_value.is_some() {
                Some(SensorTransform {
                    min_value: self.min_value.unwrap(),
                    max_value: self.max_value.unwrap(),
                })
            } else {
                None
            },
        }
    }
}

pub(crate) async fn get_sensors(db: &PgPool) -> sqlx::Result<Vec<Sensor>> {
    let db_sensors: Vec<SensorRow> = sqlx::query_as(
        r#"
        SELECT s.id, s.name, t.min_value, t.max_value
        FROM sensor AS s
        LEFT JOIN sensor_transform AS t ON t.sensor_id = s.id;
    "#,
    )
    .fetch_all(db)
    .await?;

    let sensors: Vec<Sensor> = db_sensors.into_iter().map(|s| s.into()).collect();
    Ok(sensors)
}

pub(crate) async fn get_sensors_in_group(group_name: &str, db: &PgPool) -> sqlx::Result<Vec<Sensor>> {
    let db_sensors: Vec<SensorRow> = sqlx::query_as(
        r#"
        SELECT s.id, s.name, t.min_value, t.max_value
        FROM sensor AS s
        LEFT JOIN sensor_transform AS t ON t.sensor_id = s.id
        LEFT JOIN sensor_grouping AS g ON g.sensor_id = s.id
        WHERE g.group_name = $1
    "#,
    )
    .bind(group_name)
    .fetch_all(db)
    .await?;

    let sensors: Vec<Sensor> = db_sensors.into_iter().map(|s| s.into()).collect();
    Ok(sensors)
}

pub(crate) async fn get_latest_reading(sensor_id: i32, db_pool: &PgPool) -> sqlx::Result<Reading> {
    let reading: Reading = sqlx::query_as(
        r#"
        SELECT timestamp, value
        FROM reading
        WHERE sensor_id = $1 AND aggregation = 0
        ORDER BY timestamp DESC
        LIMIT 1;
    "#,
    )
    .bind(sensor_id)
    .fetch_one(db_pool)
    .await?;

    Ok(reading)
}

pub(crate) async fn get_sensor_transform(
    sensor_id: i32,
    db_pool: &PgPool,
) -> sqlx::Result<Option<SensorTransform>> {
    let db_transform: Option<(f64, f64)> = sqlx::query_as(
        r#"
        SELECT min_value, max_value
        FROM sensor_transform
        WHERE sensor_id = $1
    "#,
    )
    .bind(sensor_id)
    .fetch_optional(db_pool)
    .await
    .unwrap();

    let transform = if let Some((min_value, max_value)) = db_transform {
        Some(crate::models::SensorTransform {
            min_value,
            max_value,
        })
    } else {
        None
    };

    Ok(transform)
}

pub(crate) async fn get_first_reading(db_pool: &PgPool, sensor_id: i32) -> Option<Reading> {
    sqlx::query_as(
        r#"
        SELECT timestamp, value
        FROM reading
        WHERE sensor_id = $1
        ORDER BY timestamp ASC
        LIMIT 1
    "#,
    )
    .bind(sensor_id)
    .fetch_optional(db_pool)
    .await
    .unwrap()
}

pub(crate) async fn get_last_reading(db_pool: &PgPool, sensor_id: i32) -> Reading {
    sqlx::query_as(
        r#"
        SELECT timestamp, value
        FROM reading
        WHERE sensor_id = $1
        ORDER BY timestamp DESC
        LIMIT 1
    "#,
    )
    .bind(sensor_id)
    .fetch_one(db_pool)
    .await
    .unwrap()
}

pub(crate) async fn get_readings_after(sensor_id: i32, after: DateTime<Utc>, aggregation: Aggregation, db_pool: &PgPool) -> sqlx::Result<Vec<Reading>> {
    let after_timestamp = after.naive_utc();
    let readings: Vec<Reading> = sqlx::query_as(
        r#"
        SELECT timestamp, value
        FROM reading
        WHERE sensor_id = $1 AND aggregation = $2 AND timestamp > $3
        ORDER BY timestamp DESC
    "#,
    )
    .bind(sensor_id)
    .bind(aggregation)
    .bind(after_timestamp)
    .fetch_all(db_pool)
    .await?;

    Ok(readings)
}
