use crate::models::SensorReadingsQuery;

use super::handlers;
use sqlx::postgres::PgPool;
use warp::Filter;

pub fn sensors(
    db_pool: PgPool,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    get_sensors(db_pool.clone())
        .or(save_sensor(db_pool.clone()))
        .or(get_latest_sensor_reading(db_pool.clone()))
        .or(get_sensor_readings(db_pool.clone()))
        .or(readings_list(db_pool.clone()))
        .or(should_water(db_pool.clone()))
}

pub fn get_sensors(
    db_pool: PgPool,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("sensors")
        .and(warp::get())
        .and(warp::any().map(move || db_pool.clone()))
        .and_then(handlers::sensor::get_sensors)
}

pub fn save_sensor(
    db_pool: PgPool,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("sensors" / i32)
        .and(warp::put())
        .and(warp::body::json())
        .and(warp::any().map(move || db_pool.clone()))
        .and_then(handlers::sensor::update_sensor)
}

pub fn get_latest_sensor_reading(
    db_pool: PgPool,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("sensors" / i32 / "latest-reading")
        .and(warp::get())
        .and(warp::any().map(move || db_pool.clone()))
        .and_then(handlers::reading::get_latest_reading)
}

pub fn get_sensor_readings(
    db_pool: PgPool,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("sensors" / i32 / "readings")
        .and(warp::get())
        .and(warp::query::<SensorReadingsQuery>())
        .and(warp::any().map(move || db_pool.clone()))
        .and_then(handlers::reading::get_sensor_readings)
}

pub fn readings_list(
    db_pool: PgPool,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("readings")
        .and(warp::get())
        .and(warp::any().map(move || db_pool.clone()))
        .and_then(handlers::reading::readings)
}

pub fn should_water(
    db_pool: PgPool,
) -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    warp::path!("should_water")
        .and(warp::get())
        .and(warp::any().map(move || db_pool.clone()))
        .and_then(handlers::watering::should_water)
}
