#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# The MIT License (MIT)
#
# Grove Base Hat for the Raspberry Pi, used to connect grove sensors.
# Copyright (C) 2018  Seeed Technology Co.,Ltd.
# Copyright (C) 2020-2021  Magnus Bjerke Vik
from datetime import datetime
from grove.helper import SlotHelper
from enum import Enum
import psycopg2
import math
import sys
import time
import smbus2 as smbus
import RPi.GPIO as RPI_GPIO
import requests
import seeed_dht
from grove.adc import ADC
from rpi_ws281x import PixelStrip, Color
from grove.i2c import Bus
from grove.gpio import GPIO

__all__ = ["GroveMoistureSensor", 'GroveWS2813RgbStrip', 'PixelStrip', 'Color']

# LED strip configuration
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
# True to invert the signal (when using NPN transistor level shift)
LED_INVERT = False

class GroveRelay(GPIO):
    def __init__(self, pin):
        super(GroveRelay, self).__init__(pin, GPIO.OUT)

    def on(self):
        self.write(1)

    def off(self):
        self.write(0)


class GroveMoistureSensor:
    '''
    Grove Moisture Sensor class

    Args:
        pin(int): number of analog pin/channel the sensor connected.
    '''

    def __init__(self, channel):
        self.channel = channel
        self.adc = ADC()

    @property
    def moisture(self):
        '''
        Get the moisture strength value/voltage

        Returns:
            (int): voltage, in mV
        '''
        value = self.adc.read_voltage(self.channel)
        return value


class GroveWS2813RgbStrip(PixelStrip):
    '''
    Wrapper Class for Grove - WS2813 RGB LED Strip Waterproof - XXX LED/m

    Args:
        pin(int)  : 12, 18 for RPi
        count(int): strip LEDs count
        brightness(int): optional, set to 0 for darkest and 255 for brightest, default 255
    '''

    def __init__(self, pin, count, brightness=None):
        ws2812_pins = {12: 0, 13: 1, 18: 0, 19: 1}
        if not pin in ws2812_pins.keys():
            print("OneLedTypedWs2812: pin {} could not used with WS2812".format(pin))
            return
        channel = ws2812_pins.get(pin)

        if brightness is None:
            brightness = LED_BRIGHTNESS

        # Create PixelStrip object with appropriate configuration.
        super(GroveWS2813RgbStrip, self).__init__(
            count,
            pin,
            LED_FREQ_HZ,
            LED_DMA,
            LED_INVERT,
            brightness,
            channel
        )

        # Intialize the library (must be called once before other functions).
        self.begin()

    def color_wipe(self, color, wait_ms=50):
        """Wipe color across display a pixel at a time."""
        for i in range(self.numPixels()):
            self.setPixelColor(i, color)
            self.show()
            time.sleep(wait_ms/1000.0)


TSL2561_Control = 0x80
TSL2561_Timing = 0x81
TSL2561_Interrupt = 0x86
TSL2561_Channel0L = 0x8C
TSL2561_Channel0H = 0x8D
TSL2561_Channel1L = 0x8E
TSL2561_Channel1H = 0x8F
TSL2561_Address = 0x29  # device address

LUX_SCALE = 14  # scale by 2^14
RATIO_SCALE = 9  # scale ratio by 2^9
CH_SCALE = 10  # scale channel values by 2^10
CHSCALE_TINT0 = 0x7517  # 322/11 * 2^CH_SCALE
CHSCALE_TINT1 = 0x0fe7  # 322/81 * 2^CH_SCALE

K1T = 0x0040  # 0.125 * 2^RATIO_SCALE
B1T = 0x01f2  # 0.0304 * 2^LUX_SCALE
M1T = 0x01be  # 0.0272 * 2^LUX_SCALE
K2T = 0x0080  # 0.250 * 2^RATIO_SCA
B2T = 0x0214  # 0.0325 * 2^LUX_SCALE
M2T = 0x02d1  # 0.0440 * 2^LUX_SCALE
K3T = 0x00c0  # 0.375 * 2^RATIO_SCALE
B3T = 0x023f  # 0.0351 * 2^LUX_SCALE
M3T = 0x037b  # 0.0544 * 2^LUX_SCALE
K4T = 0x0100  # 0.50 * 2^RATIO_SCALE
B4T = 0x0270  # 0.0381 * 2^LUX_SCALE
M4T = 0x03fe  # 0.0624 * 2^LUX_SCALE
K5T = 0x0138  # 0.61 * 2^RATIO_SCALE
B5T = 0x016f  # 0.0224 * 2^LUX_SCALE
M5T = 0x01fc  # 0.0310 * 2^LUX_SCALE
K6T = 0x019a  # 0.80 * 2^RATIO_SCALE
B6T = 0x00d2  # 0.0128 * 2^LUX_SCALE
M6T = 0x00fb  # 0.0153 * 2^LUX_SCALE
K7T = 0x029a  # 1.3 * 2^RATIO_SCALE
B7T = 0x0018  # 0.00146 * 2^LUX_SCALE
M7T = 0x0012  # 0.00112 * 2^LUX_SCALE
K8T = 0x029a  # 1.3 * 2^RATIO_SCALE
B8T = 0x0000  # 0.000 * 2^LUX_SCALE
M8T = 0x0000  # 0.000 * 2^LUX_SCALE


K1C = 0x0043  # 0.130 * 2^RATIO_SCALE
B1C = 0x0204  # 0.0315 * 2^LUX_SCALE
M1C = 0x01ad  # 0.0262 * 2^LUX_SCALE
K2C = 0x0085  # 0.260 * 2^RATIO_SCALE
B2C = 0x0228  # 0.0337 * 2^LUX_SCALE
M2C = 0x02c1  # 0.0430 * 2^LUX_SCALE
K3C = 0x00c8  # 0.390 * 2^RATIO_SCALE
B3C = 0x0253  # 0.0363 * 2^LUX_SCALE
M3C = 0x0363  # 0.0529 * 2^LUX_SCALE
K4C = 0x010a  # 0.520 * 2^RATIO_SCALE
B4C = 0x0282  # 0.0392 * 2^LUX_SCALE
M4C = 0x03df  # 0.0605 * 2^LUX_SCALE
K5C = 0x014d  # 0.65 * 2^RATIO_SCALE
B5C = 0x0177  # 0.0229 * 2^LUX_SCALE
M5C = 0x01dd  # 0.0291 * 2^LUX_SCALE
K6C = 0x019a  # 0.80 * 2^RATIO_SCALE
B6C = 0x0101  # 0.0157 * 2^LUX_SCALE
M6C = 0x0127  # 0.0180 * 2^LUX_SCALE
K7C = 0x029a  # 1.3 * 2^RATIO_SCALE
B7C = 0x0037  # 0.00338 * 2^LUX_SCALE
M7C = 0x002b  # 0.00260 * 2^LUX_SCALE
K8C = 0x029a  # 1.3 * 2^RATIO_SCALE
B8C = 0x0000  # 0.000 * 2^LUX_SCALE
M8C = 0x0000  # 0.000 * 2^LUX_SCALE

debug = True


def log_debug(msg):
    if debug:
        print(msg)


class GroveDigitalLightSensor:
    def __init__(self, bus=None):
        if bus is None:
            # use the bus that matches your raspi version
            rev = RPI_GPIO.RPI_INFO['P1_REVISION']
            if rev == 2 or rev == 3:
                bus = 1  # for Pi 2+
            else:
                bus = 0
        self.bus = smbus.SMBus(bus)

        self.ready = False
        self.address = TSL2561_Address
        self.gain = 0
        self.timing = 2
        self.gain_m = 1
        self.timing_ms = 0.0
        self.package_type = 0
        self.cooldown_time = 0.005  # seconds

        try:
            self.power_up()
            self.ready = True
        except:
            print("Failed to power up light sensor.")
            return

        self.set_tint_and_gain()
        self.write_byte(TSL2561_Interrupt, 0x00)
        self.power_down()

    def read_byte(self, address):
        value = self.bus.read_byte_data(self.address, address)
        time.sleep(self.cooldown_time)
        return value

    def write_byte(self, address, data):
        self.bus.write_byte_data(self.address, address, data)
        time.sleep(self.cooldown_time)

    def power_up(self):
        self.write_byte(TSL2561_Control, 0x03)

    def power_down(self):
        self.write_byte(TSL2561_Control, 0x00)

    def set_tint_and_gain(self):
        if self.gain == 0:
            self.gain_m = 1
        else:
            self.gain_m = 16

        if self.timing == 0:
            self.timing_ms = 13.7
        elif self.timing == 1:
            self.timing_ms = 101.0
        else:
            self.timing_ms = 402.0

        self.write_byte(TSL2561_Timing, self.timing | self.gain << 4)

    def read_channels(self):
        # Must sleep for 1 second for integration to be done (don't know why)
        sleep_length = 1.0
        time.sleep(sleep_length)

        ch0_low = self.read_byte(TSL2561_Channel0L)
        ch0_high = self.read_byte(TSL2561_Channel0H)
        ch1_low = self.read_byte(TSL2561_Channel1L)
        ch1_high = self.read_byte(TSL2561_Channel1H)

        ch0 = (ch0_high << 8) | ch0_low
        ch1 = (ch1_high << 8) | ch1_low

        log_debug("TSL2561.readVisibleLux: channel 0 = %i, channel 1 = %i [gain=%ix, timing=%ims]" % (
            ch0, ch1, self.gain_m, self.timing_ms))
        time.sleep(self.cooldown_time)

        return (ch0, ch1)

    def read_visible_lux(self):
        self.power_up()
        (ch0, ch1) = self.read_channels()

        if ch0 < 500 and self.timing == 0:
            self.timing = 1
            log_debug(
                "TSL2561.readVisibleLux: too dark. Increasing integration time from 13.7ms to 101ms")
            time.sleep(self.cooldown_time)
            self.set_tint_and_gain()
            (ch0, ch1) = self.read_channels()

        if ch0 < 500 and self.timing == 1:
            self.timing = 2
            log_debug(
                "TSL2561.readVisibleLux: too dark. Increasing integration time from 101ms to 402ms")
            time.sleep(self.cooldown_time)
            self.set_tint_and_gain()
            (ch0, ch1) = self.read_channels()

        if ch0 < 500 and self.timing == 2 and self.gain == 0:
            self.gain = 1
            log_debug("TSL2561.readVisibleLux: too dark. Setting high gain")
            time.sleep(self.cooldown_time)
            self.set_tint_and_gain()
            (ch0, ch1) = self.read_channels()

        if (ch0 > 20000 or ch1 > 20000) and self.timing == 2 and self.gain == 1:
            self.gain = 0
            log_debug("TSL2561.readVisibleLux: enough light. Setting low gain")
            time.sleep(self.cooldown_time)
            self.set_tint_and_gain()
            (ch0, ch1) = self.read_channels()

        if (ch0 > 20000 or ch1 > 20000) and self.timing == 2:
            self.timing = 1
            log_debug(
                "TSL2561.readVisibleLux: enough light. Reducing integration time from 402ms to 101ms")
            time.sleep(self.cooldown_time)
            self.set_tint_and_gain()
            (ch0, ch1) = self.read_channels()

        if (ch0 > 10000 or ch1 > 10000) and self.timing == 1:
            self.timing = 0
            log_debug(
                "TSL2561.readVisibleLux: enough light. Reducing integration time from 101ms to 13.7ms")
            time.sleep(self.cooldown_time)
            self.set_tint_and_gain()
            (ch0, ch1) = self.read_channels()

        self.power_down()

        if (self.timing == 0 and (ch0 > 5000 or ch1 > 5000)) or (self.timing == 1 and (ch0 > 37000 or ch1 > 37000)) or (self.timing == 2 and (ch0 > 65000 or ch1 > 65000)):
            # overflow
            log_debug("TSL2561.readVisibleLux: Channel value is too large")
            return None

        return self.calculate_lux(ch0, ch1)

    def calculate_lux(self, ch0, ch1):
        chScale = 0

        if self.timing == 0:   # 13.7 msec
            chScale = CHSCALE_TINT0
        elif self.timing == 1:  # 101 msec
            chScale = CHSCALE_TINT1
        else:           # assume no scaling
            chScale = (1 << CH_SCALE)

        if self.gain == 0:
            chScale = chScale << 4  # scale 1X to 16X

        # scale the channel values
        sch0 = (ch0 * chScale) >> CH_SCALE
        sch1 = (ch1 * chScale) >> CH_SCALE

        ratio = 0
        if sch0 != 0:
            ratio = (sch1 << (RATIO_SCALE+1)) // sch0
        ratio = (ratio + 1) >> 1

        if self.package_type == 0:  # T package
            if ((ratio >= 0) and (ratio <= K1T)):
                b = B1T
                m = M1T
            elif (ratio <= K2T):
                b = B2T
                m = M2T
            elif (ratio <= K3T):
                b = B3T
                m = M3T
            elif (ratio <= K4T):
                b = B4T
                m = M4T
            elif (ratio <= K5T):
                b = B5T
                m = M5T
            elif (ratio <= K6T):
                b = B6T
                m = M6T
            elif (ratio <= K7T):
                b = B7T
                m = M7T
            elif (ratio > K8T):
                b = B8T
                m = M8T
        elif self.package_type == 1:  # CS package
            if ((ratio >= 0) and (ratio <= K1C)):
                b = B1C
                m = M1C
            elif (ratio <= K2C):
                b = B2C
                m = M2C
            elif (ratio <= K3C):
                b = B3C
                m = M3C
            elif (ratio <= K4C):
                b = B4C
                m = M4C
            elif (ratio <= K5C):
                b = B5C
                m = M5C
            elif (ratio <= K6C):
                b = B6C
                m = M6C
            elif (ratio <= K7C):
                b = B7C
                m = M7C

        temp = ((sch0*b)-(sch1*m))

        if temp < 0:
            temp = 0
        temp += (1 << (LUX_SCALE-1))

        # strip off fractional portion
        lux = temp >> LUX_SCALE

        time.sleep(self.cooldown_time)

        return lux


def scale_color(color, fraction):
    return (int(color[0] * fraction), int(color[1] * fraction), int(color[2] * fraction))


class MoistureLevel(Enum):
    DRY = 0
    MOIST = 1
    WET = 2


class Moisture:
    def __init__(self, value):
        self.value = value
        self.level = Moisture.level_from_value(value)

    @staticmethod
    def level_from_value(value):
        if value >= 1800:
            return MoistureLevel.DRY
        elif value >= 1500:
            return MoistureLevel.MOIST
        else:
            return MoistureLevel.WET

    def __str__(self):
        return "({0}, {1})".format(self.value, self.level)


def check_adc_pin(pin):
    sh_adc = SlotHelper(SlotHelper.ADC)
    if not sh_adc.is_adapted(pin):
        sh_adc.list_avail()
        sys.exit(1)

def should_water():
    try:
        r = requests.get('http://localhost:3030/should_water')
    except:
        print("Failed to connect to API")
        return False
    if r.status_code != 200:
        print("Got unknown status code from API: {}".format(r.status_code))
        return False
    return r.json()

def main():
    sh_pwm = SlotHelper(SlotHelper.PWM)

    pin_moisture_sensor_1 = 0
    pin_moisture_sensor_2 = 2
    pin_moisture_sensor_3 = 4
    pin_moisture_sensor_4 = 6
    pin_strip = 18
    pin_relay = 5
    pin_temp = 12
    led_count = 20

    check_adc_pin(pin_moisture_sensor_1)
    check_adc_pin(pin_moisture_sensor_2)
    check_adc_pin(pin_moisture_sensor_3)
    check_adc_pin(pin_moisture_sensor_4)

    if not sh_pwm.is_adapted(pin_strip):
        sh_pwm.list_avail()
        sys.exit(1)

    moisture_sensor_1 = GroveMoistureSensor(pin_moisture_sensor_1)
    moisture_sensor_2 = GroveMoistureSensor(pin_moisture_sensor_2)
    moisture_sensor_3 = GroveMoistureSensor(pin_moisture_sensor_3)
    moisture_sensor_4 = GroveMoistureSensor(pin_moisture_sensor_4)

    temp_sensor = seeed_dht.DHT("11", 12)

    strip = GroveWS2813RgbStrip(pin_strip, led_count)
    light_sensor = GroveDigitalLightSensor()
    color = (0, 0, 0)

    relay = GroveRelay(pin_relay)
    relay_on = False

    db_conn = psycopg2.connect(
        "dbname=plantmonitor user=plantmonitor password=738LbZD4cT6dYSnSjDtQwil5")
    db_cursor = db_conn.cursor()

    print('Press Ctrl-C to quit.')
    try:
        while True:
            moisture_1 = Moisture(moisture_sensor_1.moisture)
            moisture_2 = Moisture(moisture_sensor_2.moisture)
            moisture_3 = Moisture(moisture_sensor_3.moisture)
            moisture_4 = Moisture(moisture_sensor_4.moisture)

            avg_moisture = Moisture(
                (moisture_1.value + moisture_2.value + moisture_3.value + moisture_4.value) / 4)
                #(moisture_1.value + moisture_2.value + moisture_4.value) / 3)

            if avg_moisture.level == MoistureLevel.DRY:
                new_color = (255, 0, 0)
            elif avg_moisture.level == MoistureLevel.MOIST:
                new_color = (255, 180, 0)
            else:
                new_color = (0, 255, 0)

            print('Moisture   1: {0}'.format(moisture_1))
            print('Moisture   2: {0}'.format(moisture_2))
            print('Moisture   3: {0}'.format(moisture_3))
            print('Moisture   4: {0}'.format(moisture_4))
            print('Moisture AVG: {0}'.format(avg_moisture))

            lux = None
            if light_sensor.ready:
                lux = light_sensor.read_visible_lux()

            print('Lux: {0}'.format(lux))

            led_brightness_fraction = 1.0
            if (lux is not None):
                led_brightness_fraction = max(0.05, min(1.0, lux / 10000))
            # led_brightness = max(0, min(255, int(5 + led_brightness_fraction * 255)))
            print('LED brightness: {0}'.format(led_brightness_fraction))

            # strip.setBrightness(led_brightness)
            # new_color = scale_color(new_color, led_brightness_fraction)
            # print('LED color: {0}'.format(new_color))
            # if (new_color != color):
            #     strip.color_wipe(
            #         Color(new_color[0], new_color[1], new_color[2]), 20)
            #     color = new_color

            humidity, temperature = temp_sensor.read()
            print('Humidity {0:.1f}%, Temperature {1:.1f}*'.format(humidity, temperature))

            timestamp = datetime.utcnow()
            db_cursor.execute(
               "INSERT INTO reading (sensor_id, timestamp, value) VALUES (%s, %s, %s)", (1, timestamp, moisture_1.value))
            db_cursor.execute(
               "INSERT INTO reading (sensor_id, timestamp, value) VALUES (%s, %s, %s)", (3, timestamp, moisture_2.value))
            db_cursor.execute(
               "INSERT INTO reading (sensor_id, timestamp, value) VALUES (%s, %s, %s)", (4, timestamp, moisture_3.value))
            db_cursor.execute(
               "INSERT INTO reading (sensor_id, timestamp, value) VALUES (%s, %s, %s)", (5, timestamp, moisture_4.value))
            if (lux is not None):
               db_cursor.execute(
                   "INSERT INTO reading (sensor_id, timestamp, value) VALUES (%s, %s, %s)", (2, timestamp, lux))
            db_cursor.execute(
               "INSERT INTO reading (sensor_id, timestamp, value) VALUES (%s, %s, %s)", (7, timestamp, temperature))
            db_cursor.execute(
               "INSERT INTO reading (sensor_id, timestamp, value) VALUES (%s, %s, %s)", (8, timestamp, humidity))
            db_conn.commit()

            relay_on = should_water()
            if relay_on:
                relay.on()
            else:
                relay.off()

            time.sleep(2)

    except KeyboardInterrupt:
        # clear all leds when exit
        strip.color_wipe(Color(0, 0, 0), 10)

    finally:
        db_cursor.close()
        db_conn.close()


if __name__ == '__main__':
    main()
