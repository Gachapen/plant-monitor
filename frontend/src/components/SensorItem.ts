import { css, CSSResultGroup, html, LitElement, PropertyValues, TemplateResult } from 'lit';
import { customElement, property, state } from 'lit/decorators.js';
import { until } from 'lit/directives/until.js';
import { apiClient, Reading, Sensor } from '../services/ApiClient.js';
import './Card.js';
import './Loader.js';

function renderReading(reading: Reading): TemplateResult {
  return html`${reading.value.toFixed(2)} (${reading.rawValue})
  (${reading.timestamp.toLocaleTimeString()}
  ${reading.timestamp.toLocaleDateString()})`;
}

@customElement('sensor-item')
export class SensorItem extends LitElement {
  @property({ type: Object })
  sensor: Sensor = null!;

  @state()
  _name = '';

  @state()
  _transform = false;

  @state()
  _minValue = 0;

  @state()
  _maxValue = 0;

  @state()
  _readingPromise?: Promise<TemplateResult> = undefined;

  static get styles(): CSSResultGroup {
    return css`
      form > * {
        display: block;
      }

      form > *:not(:first-child) {
        margin-top: 1rem;
      }

      input {
        padding: 0.5rem;
      }

      button {
        padding: 0.5rem 1rem;
      }
    `;
  }

  constructor() {
    super();
    setInterval(async () => await this.reloadReading(), 2000);
  }

  updated(changedProperties: PropertyValues): void {
    if (changedProperties.has('sensor')) {
      this._transform = this.sensor.transform !== undefined;
      this._minValue = this.sensor.transform?.minValue ?? 0;
      this._maxValue = this.sensor.transform?.maxValue ?? 1;
      this._name = this.sensor.name;

      const previousSensor = changedProperties.get('sensor') as Sensor;
      if (!previousSensor || previousSensor.id !== this.sensor.id) {
        this._readingPromise = apiClient
          .getLatestReading(this.sensor.id)
          .then(renderReading);
      }
    }
  }

  render(): TemplateResult {
    return html`
      <pm-card>
        <h3 slot="header">${this._name}</h3>
        <div slot="body">
          <p>
            Reading:
            ${until(this._readingPromise, html`<pm-loader></pm-loader>`)}
          </p>
          <form action="javascript:void(0);" @submit=${this.saveSensor}>
            <label>
              Name:
              <input
                type="text"
                .value=${this._name}
                @input=${this.onNameChanged}
              />
            </label>

            <label>
              <input
                type="checkbox"
                .checked=${this._transform}
                @change=${this.onTransformChecked}
              />
              Transform
            </label>

            ${this._transform
              ? html`
                  <label>
                    Minimum:
                    <input
                      type="number"
                      .value=${this._minValue.toString() ?? ''}
                      @input=${this.onMinValueChanged}
                    />
                  </label>

                  <label>
                    Maximum:
                    <input
                      type="number"
                      .value=${this._maxValue.toString() ?? ''}
                      @input=${this.onMaxValueChanged}
                    />
                  </label>
                `
              : ''}

            <button type="submit">Save</button>
          </form>
        </div>
      </pm-card>
    `;
  }

  onNameChanged(event: Event): void {
    const { target } = event;
    if (!(target instanceof HTMLInputElement)) {
      throw new Error('wrong element type');
    }

    this._name = target.value;
    this.fireChangeEvent();
  }

  onTransformChecked(event: Event): void {
    const { target } = event;
    if (!(target instanceof HTMLInputElement)) {
      throw new Error('wrong element type');
    }
    this._transform = target.checked;
    this.fireChangeEvent();
  }

  onMinValueChanged(event: Event): void {
    const { target } = event;
    if (!(target instanceof HTMLInputElement)) {
      throw new Error('wrong element type');
    }

    this._minValue = parseFloat(target.value);
    this.fireChangeEvent();
  }

  onMaxValueChanged(event: Event): void {
    const { target } = event;
    if (!(target instanceof HTMLInputElement)) {
      throw new Error('wrong element type');
    }

    this._maxValue = parseFloat(target.value);
    this.fireChangeEvent();
  }

  fireChangeEvent(): void {
    const sensor: Sensor = {
      ...this.sensor,
      name: this._name,
      transform: this._transform
        ? {
            minValue: this._minValue,
            maxValue: this._maxValue,
          }
        : undefined,
    };

    this.dispatchEvent(
      new CustomEvent('change', {
        detail: {
          sensor,
        },
      })
    );
  }

  async saveSensor(): Promise<void> {
    await apiClient.saveSensor(this.sensor);
  }

  async reloadReading(): Promise<void> {
    const reading = await apiClient.getLatestReading(this.sensor.id);
    this._readingPromise = Promise.resolve(renderReading(reading));
  }
}
