import { html, LitElement, TemplateResult } from 'lit';
import { customElement } from 'lit/decorators.js';

@customElement('pm-loader')
export class Loader extends LitElement {
  render(): TemplateResult {
    return html`Loading...`;
  }
}
