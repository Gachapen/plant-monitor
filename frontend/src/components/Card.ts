import { css, CSSResultGroup, html, LitElement, TemplateResult } from 'lit';
import { customElement } from 'lit/decorators.js';

@customElement('pm-card')
export class Card extends LitElement {
  static get styles(): CSSResultGroup {
    return css`
      :host {
        display: block;
        margin: 0.5rem;
      }

      .main {
        background: white;
        padding: 1.5rem;
        border-radius: 1px;
        border-top: 3px solid gray;
      }

      header ::slotted(*) {
        margin-top: 0;
      }
    `;
  }

  render(): TemplateResult {
    return html`
      <div class="main">
        <header><slot name="header"></slot></header>
        <article><slot name="body"></slot></article>
      </div>
    `;
  }
}
