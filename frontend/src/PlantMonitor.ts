import { LitElement, html, css, TemplateResult } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import {
  Chart,
  ChartData,
  ChartDataset,
  Legend,
  LinearScale,
  LineController,
  LineElement,
  PointElement,
  TimeScale,
  Title,
  Tooltip,
} from 'chart.js';
import 'chartjs-adapter-date-fns';
import {
  Aggregation,
  apiClient,
  LatestSensorReading,
  Sensor,
  SensorReadings,
} from './services/ApiClient.js';
import { setupFakeApiClient } from './api_faker.js';
import './components/SensorItem.js';
import { replaceArrayElement } from './common.js';

Chart.register(
  LineElement,
  PointElement,
  LineController,
  LinearScale,
  TimeScale,
  Legend,
  Title,
  Tooltip
);

setupFakeApiClient(['getLatestReadings']);

const colors = [
  '#556b2f',
  '#191970',
  '#ff4500',
  '#ffd700',
  '#00ff00',
  '#00bfff',
  '#0000ff',
  '#ff1493',
];

function GetRandomColor(): string {
  const index = Math.floor(Math.random() * colors.length);
  const color = colors[index];
  colors.splice(index, 1);
  return color;
}

function getYAxisId(sensorName: string): string {
  switch (sensorName) {
    case 'Temperature':
      return 'yTemperature';
    case 'Humidity':
      return 'yHumidity';
    default:
      return 'y';
  }
}

@customElement('plant-monitor')
export class PlantMonitor extends LitElement {
  @property({ type: Array }) sensors: Sensor[] = [];
  @property({ type: Array }) readings: SensorReadings[] = [];
  @property({ type: Array }) liveReadings: LatestSensorReading[] = [];

  private chart: Chart | null = null;

  static styles = css`
    :host {
      min-height: 100vh;
      display: flex;
      flex-direction: column;
      align-items: center;
      justify-content: flex-start;
      color: #1a2b42;
      margin: 0 auto;
    }

    main {
      flex-grow: 1;
      width: 100%;
    }

    h1,
    h2 {
      text-align: center;
    }

    #readings {
      max-width: 1000px;
      margin: 0 auto;
    }

    .sensor-grid {
      display: grid;
      grid-template-columns: 1fr;
    }

    @media only screen and (min-width: 768px) {
      .sensor-grid {
        grid-template-columns: 1fr 1fr;
      }
    }

    @media only screen and (min-width: 1200px) {
      .sensor-grid {
        grid-template-columns: 1fr 1fr 1fr;
      }
    }

    @media only screen and (min-width: 1600px) {
      .sensor-grid {
        grid-template-columns: 1fr 1fr 1fr 1fr;
      }
    }

    @media only screen and (min-width: 2000px) {
      .sensor-grid {
        grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
      }
    }
  `;

  render(): TemplateResult {
    return html`
      <main>
        <h1>Plant Monitor</h1>

        <section id="readings">
          <header><h2>Readings</h2></header>
          <canvas id="chart" width="800" height="400"></canvas>
        </section>

        <section id="sensors">
          <header><h2>Sensors</h2></header>

          <div class="sensor-grid">
            ${this.sensors.map(
              (s, index) =>
                html`<sensor-item
                  .sensor=${s}
                  @change=${(e: CustomEvent) => this.onSensorChanged(e, index)}
                ></sensor-item>`
            )}
          </div>
        </section>
      </main>
    `;
  }

  async firstUpdated(): Promise<void> {
    await this.updateSensors();
    await this.updateReadingsHistory();
  }

  get chartData(): ChartData {
    return {
      datasets: this.readings.map(
        (s): ChartDataset => ({
          label: s.name,
          yAxisID: getYAxisId(s.name),
          borderColor: GetRandomColor(),
          backgroundColor: 'rgba(0, 0, 0, 0)',
          data: s.readings.map(r => ({ x: r.timestamp as any, y: r.value })),
        })
      ),
    };
  }

  async updateSensors(): Promise<void> {
    this.sensors = await apiClient.getSensors();
    this.sensors.sort((a, b) => a.name.localeCompare(b.name));
  }

  async updateReadingsHistory(): Promise<void> {
    const after = new Date();
    after.setDate(after.getDate() - 3);
    // after.setHours(after.getHours() - 12);
    // after.setMinutes(after.getMinutes() - 10);

    const aggregation: Aggregation = 'HourlyAverage';
    // const aggregation: Aggregation = 'MinutelyAverage';
    // const aggregation: Aggregation = 'None';
    const sensorFilter = ['Moist 1', 'Temperature', 'Humidity'];

    const promises = this.sensors
      .filter(s => sensorFilter.includes(s.name))
      .map(async s => ({
        readings: await apiClient.getSensorReadingsAfter(
          s.id,
          after,
          aggregation
        ),
        name: s.name,
      }));
    this.readings = await Promise.all(promises);

    const canvas = this.shadowRoot?.getElementById(
      'chart'
    ) as HTMLCanvasElement;
    const data = this.chartData;

    this.chart = new Chart(canvas.getContext('2d')!, {
      type: 'line',
      data,
      options: {
        scales: {
          x: {
            type: 'time',
            time: {
              unit: 'hour',
              displayFormats: {
                hour: 'HH',
              },
            },
          },
          y: {
            suggestedMin: 0,
            suggestedMax: 1,
          },
          yTemperature: {
            suggestedMin: 0,
            suggestedMax: 40,
          },
          yHumidity: {
            suggestedMin: 0,
            suggestedMax: 100,
          },
        },
      },
    });
  }

  onSensorChanged(e: CustomEvent, index: number): void {
    const { sensor } = e.detail;
    this.sensors = replaceArrayElement(this.sensors, index, sensor);
  }
}
