import {
  apiClient,
  LatestSensorReading,
  Sensor,
  SensorReadings,
} from './services/ApiClient.js';

const fakes: Record<string, any> = {
  getSensors: async function (): Promise<Sensor[]> {
    return [
      {
        id: 1,
        name: 'Sensor 1',
      },
      {
        id: 2,
        name: 'Sensor 2',
        transform: {
          minValue: 1500,
          maxValue: 2000,
        },
      },
      {
        id: 3,
        name: 'Sensor 3',
        transform: {
          minValue: 100,
          maxValue: 500,
        },
      },
      {
        id: 4,
        name: 'Sensor 4',
      },
    ];
  },
  getLatestReadings: async function (): Promise<LatestSensorReading[]> {
    return [];
  },
  getReadings: async function (): Promise<SensorReadings[]> {
    return [];
  },
  saveSensor: async function (sensor: Sensor): Promise<void> {
    return Promise.resolve();
  },
};

type ApiClientMethodName =
  | 'getSensors'
  | 'getReadings'
  | 'getLatestReadings'
  | 'saveSensor';

export function setupFakeApiClient(
  fakedMethodNames: ApiClientMethodName[]
): void {
  for (const methodName of fakedMethodNames) {
    console.log(`Using fake ApiClient method '${methodName}'`);
    apiClient[methodName] = async (...args: any[]) => {
      console.log(`calling fake ApiClient.${methodName}`);
      return await fakes[methodName](...args);
    };
  }
}
