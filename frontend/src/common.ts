export const replaceArrayElement = <T>(
  array: T[],
  index: number,
  newElement: T
): T[] => [...array.slice(0, index), newElement, ...array.slice(index + 1)];
