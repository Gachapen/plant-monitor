export interface SensorTransform {
  minValue: number;
  maxValue: number;
}

export interface Sensor {
  id: number;
  name: string;
  transform?: SensorTransform;
}

export interface Reading {
  timestamp: Date;
  value: number;
  rawValue: number;
}

export interface SensorReadings {
  name: string;
  readings: Reading[];
}

export interface LatestSensorReading {
  name: string;
  reading: Reading;
}

export interface ReadingResponse {
  timestamp: string;
  value: number;
  rawValue: number;
}

export interface SensorReadingsResponse {
  name: string;
  readings: ReadingResponse[];
}

export interface LatestSensorReadingResponse {
  name: string;
  reading: ReadingResponse;
}

export type Aggregation =
  | 'None'
  | 'MinutelyAverage'
  | 'HourlyAverage'
  | 'DailyAverage';

export class ApiClient {
  baseUri = 'http://10.0.0.5:3030';

  async get(path: string): Promise<any> {
    const response = await fetch(`${this.baseUri}${path}`);
    if (response.status !== 200) {
      throw new Error(`Response returned status code ${response.status}`);
    }

    return await response.json();
  }

  async put(path: string, body: any): Promise<void> {
    const response = await fetch(`${this.baseUri}${path}`, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
    });

    if (response.status !== 201) {
      throw new Error(`Response returned status code ${response.status}`);
    }
  }

  async getLatestReadings(): Promise<LatestSensorReading[]> {
    const responseData: LatestSensorReadingResponse[] = await this.get(
      '/latest-readings'
    );
    const latestReadings: LatestSensorReading[] = responseData.map(s => ({
      ...s,
      reading: {
        ...s.reading,
        timestamp: new Date(s.reading.timestamp),
      },
    }));

    return latestReadings;
  }

  async getReadings(): Promise<SensorReadings[]> {
    const responseData: SensorReadingsResponse[] = await this.get('/readings');
    const sensors: SensorReadings[] = responseData.map(s => ({
      ...s,
      readings: s.readings.map(r => ({
        ...r,
        timestamp: new Date(r.timestamp),
      })),
    }));

    return sensors;
  }

  async getSensors(): Promise<Sensor[]> {
    const sensors: Sensor[] = await this.get('/sensors');
    return sensors;
  }

  async saveSensor(sensor: Sensor): Promise<void> {
    await this.put(`/sensors/${sensor.id}`, sensor);
  }

  async getLatestReading(sensorId: number): Promise<Reading> {
    const response = await this.get(`/sensors/${sensorId}/latest-reading`);
    return {
      ...response,
      timestamp: new Date(response.timestamp),
    };
  }

  async getSensorReadingsAfter(
    sensorId: number,
    after: Date,
    aggregation: Aggregation
  ): Promise<Reading[]> {
    const responseData: ReadingResponse[] = await this.get(
      `/sensors/${sensorId}/readings?after=${after.toISOString()}&aggregation=${aggregation}`
    );
    const readings: Reading[] = responseData.map(r => ({
      ...r,
      timestamp: new Date(r.timestamp),
    }));

    return readings;
  }
}

export const apiClient = new ApiClient();
