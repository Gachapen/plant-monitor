#!/bin/bash

(cd backend && cargo build --release --target=armv7-unknown-linux-gnueabihf)
# (cd backend && cargo build --target=armv7-unknown-linux-gnueabihf)

rsync --progress monitor/* backend/target/armv7-unknown-linux-gnueabihf/release/plant-monitor pi@10.0.0.5:~/
# rsync --progress monitor/plant_monitor.py backend/target/armv7-unknown-linux-gnueabihf/debug/plant-monitor pi@10.0.0.5:~/
